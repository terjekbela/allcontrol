<?php
    global $conf, $cache;

    if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") {
        header("Location: https://" . $_SERVER['HTTP_HOST'] . preg_replace('/index\.php/', '', $_SERVER['PHP_SELF']));
    } else {
        header('Content-Type: text/html; charset=UTF-8');
        session_start();
        require_once('include/allcontrol/common.php');
        $_POST['action'] && homeScreenAction();
        $screen = explode('/', $_SERVER['REQUEST_URI'])[1];
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title><?= $conf['system']['name'] ?></title>
    <meta http-equiv="xrefresh" content="60" />
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="/include/allcontrol/style/layout.css" />
    <link rel="stylesheet" href="/include/jquery.mobile/jquery.mobile.inline-png.min.css" />
    <link rel="stylesheet" href="/include/jquery.mobile/jquery.mobile.structure.min.css" />
    <script language="JavaScript" type="text/javascript" src="/include/jquery/jquery.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="/include/globalize/globalize.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="/include/chartjs/dx.chartjs.js"></script>
    <script language="JavaScript" type="text/javascript">
        $(document).bind("mobileinit", function(){
	       $.mobile.ajaxEnabled = false;
        });
    </script>
    <script src="/include/jquery.mobile/jquery.mobile.min.js"></script>
</head>
<body>

<div data-role="page">
<?php
    if($_SESSION['username']) {
        if($screen) {
            print homeScreenItems($screen);
        } else {
            print homeScreenMenu();
        }
    } else {
        print homeScreenLogin();
    }
?>
</div>

</body>
</html>
