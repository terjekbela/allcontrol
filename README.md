# Allcontrol

Highly configurable home automation system for geeks.

You can control your house heating, aquaponics system, garage door, solar pump, lighting,
or just about anything you can hook up to your PC. Allcontrol is currently under active development,
so new features are coming every week.

## Features
- Friendly web interface designed mostly for modern smartphones and tablets
- Uses secure HTTPS protocol
- (probably not so) Easily configurable I/O parameters (sensors, switches, webcams)

## Compatibility
- Reads all Dallas 1-Wire thermometers, humidity and light sensors, and the like
- Controls Dallas 1-Wire switches
- Controls embedded systems GPIO ports, like RPi, Beaglebone, or plain Linux GPIO bus
- Reads data from CurrentCost energy sensors (soon)
- Draws graphs from data gathered from the above sensors and usr activity (soon)
- Shows remote IP webcam images (soon)

## Environment
- Runs on a normal PC, or on some embedded computers like BeagleBone, RaspberryPi, OpenWRT compatible router, etc...
- Runs on most GNU/Linux operating systems, including Ubuntu, Debian, Angström, OpenWRT
- Uses jQuery and jQuery Mobile framework
- Almost any modern web browser can function as a client (desktop, phone, tablet, see jQuery Mobile compatibility chart)
- Uses Apache, MemCached
- Does not need a database to operate
- Configuration is JSON compatible

## Licensing 
- Released under the terms of the GNU General Public License (GPL v2)
