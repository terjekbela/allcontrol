<?php

global $conf;

//----------------------------------------------------------------------------------------------------
// Global
//----------------------------------------------------------------------------------------------------

$conf = json_decode('
    {
	"system": {
	    "name"    : "Site name goes here",
	    "uid"     : "5aa421dbcf8cd544ec782b831290af0a",
	    "locale"  : "hu_HU",
	    "timezone": "Europe\/Budapest"
	    "cache"   : {
		"memcache": {
		    "host": "localhost",
		    "port": 11211
		},
		"cassandra": {
		    "host": '',
		    "port": ''
		}
	    }
	},
	"sensor": {
	    "temp-study": {
		"type"      : "1wire",
		"subtype"   : "thermo",
		"unit"      : "C",
		"default"   : 20,
		"range-min" : -20,
		"range-max" : 50,
		"arch-freq" : 60,
		"arch-items": 10080,
		"file"      : "/mnt/1wire/10.6A63E4000800/temperature"
	    },
	    "linux-load": {
		"type"      : "linux",
		"subtype"   : "load",
		"unit"      : "pct",
		"default"   : 0.01,
		"range-min" : 0,
		"range-max" : 100,
		"arch-freq" : 60,
		"arch-items": 10080
	    }
	}
    }
', true);


//----------------------------------------------------------------------------------------------------
// Input
//----------------------------------------------------------------------------------------------------
$conf['input']['temp-target']['default']			= 20;



//----------------------------------------------------------------------------------------------------
// Variable
//----------------------------------------------------------------------------------------------------
$conf['variable']['temp-target']['exp']				= "date('Hi') >= \$i['day-from'] && date('Hi') <= \$i['day-to'] ? \$i['temp-day'] : \$i['temp-night']";
$conf['variable']['temp-target']['arch-freq']			= 60;
$conf['variable']['temp-target']['arch-items']			= 10080;



//----------------------------------------------------------------------------------------------------
// Output
//----------------------------------------------------------------------------------------------------
$conf['output']['switch-heater']['type']			= '1wire';
$conf['output']['switch-heater']['subtype']			= 'switch';
$conf['output']['switch-heater']['file']			= '/mnt/1wire/12.E7337D000000/PIO.A';
$conf['output']['switch-heater']['exp']				= "\$v['temp-target'] > \$s['temp-study'] ? '1' : '0'";
$conf['output']['switch-heater']['default']			= '0';
$conf['output']['switch-heater']['arch-freq']			= 60;
$conf['output']['switch-heater']['arch-items']			= 10080;



//----------------------------------------------------------------------------------------------------
// Screen
//----------------------------------------------------------------------------------------------------
$conf['screen']['thermo']['name']				= 'Heating';
$conf['screen']['thermo']['icon']				= 'star';
$conf['screen']['thermo']['items'][0]['name']			= 'Temp name strring';
$conf['screen']['thermo']['items'][0]['desc']			= 'Temp name strring';
$conf['screen']['thermo']['items'][0]['type']			= 'value';
$conf['screen']['thermo']['items'][0]['subtype']		= 'horizontal';
$conf['screen']['thermo']['items'][0]['label'][0]		= 'Study';
$conf['screen']['thermo']['items'][0]['label'][1]		= 'Living room';
$conf['screen']['thermo']['items'][0]['label'][2]		= 'Cél';
$conf['screen']['thermo']['items'][0]['label'][3]		= 'Üzemmód';
$conf['screen']['thermo']['items'][0]['exp'][0]			= "round(\$s['temp-study'], 1).'°C'";
$conf['screen']['thermo']['items'][0]['exp'][1]			= "round(\$s['temp-study'], 1).'°C'";
$conf['screen']['thermo']['items'][0]['exp'][2]			= "\$v['temp-target'].'°C'";
$conf['screen']['thermo']['items'][0]['exp'][3]			= "\$i['mode']";

$conf['screen']['stat']['name']					= 'Status';
$conf['screen']['stat']['icon']					= 'info';
$conf['screen']['stat']['items'][0]['type']			= 'status';

?>