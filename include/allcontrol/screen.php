<?php
    function homeScreenAction() {
	$log = '/var/tmp/allcontrol.log';
//	file_put_contents($log, "action: ".$_POST['action'] . "\n", FILE_APPEND | LOCK_EX);
	global $conf, $user;
	switch($_POST['action']) {
	    case 'user-login':
		if(! array_key_exists('hashalgo', $conf['system'])) {
		    $hashalgo = 'sha512';
		} else {
		    $hashalgo = $conf['system']['hashalgo'];
		}
		if($user[$_POST['username']]['password'] == hash($conf['system']['hashalgo'], $user[$_POST['username']]['salt'] . $_POST['password'])) {
		    $_SESSION['username'] = $_POST['username'];
		} else {
		    $_SESSION['username'] = '';
		}
		break;
	    case 'user-logout':
		$_SESSION['username'] = '';
		break;
	    case 'value':
		$actions = $_POST['subkey'] ? $_POST['subkey'] : $_POST['value'];
		foreach($conf['screen'][$_POST['screen']]['items'][$_POST['key']]['actions'][$actions] as $key => $action) {
		    $value = homeExpression($action['value'] ? $action['value'] : $_POST['value']);
		    switch($action['type']) {
			case 'input':
			    homeInputSetValue($action['key'], $value);
//			    file_put_contents($log, "  input: ".$action['key']." - ".$value."\n", FILE_APPEND | LOCK_EX);
			    break;
			case 'output':
			    homeOutputSetValue($action['key'], $value);
			    homeOutputWriteValue($action['key'], $value);
//			    file_put_contents($log, "  output: ".$action['key']." - ".$value."\n", FILE_APPEND | LOCK_EX);
			    break;
			case 'sleep':
			    sleep($action['value']);
//			    file_put_contents($log, "  sleep: ".$value."\n", FILE_APPEND | LOCK_EX);
			    break;
			default:
			    //
		    }
		}
		break;
	    case 'refresh':
//		...
		break;
	    default:
		//
	}
    }

    function homeScreenMenu() {
	global $conf;
	$html  = sprintf("\t<div data-role=\"header\" data-theme=\"a\">\n\t\t<h1>%s</h1>\n\t</div>\n", $conf['system']['name']);
	$html .= "\t<div data-role=\"content\">\n\t\t<div data-role=\"controlgroup\" data-type=\"vertical\" data-theme=\"b\">\n";
	$group = '';
	foreach(array_keys($conf['screen']) as $key) {
	    $screen = $conf['screen'][$key];
	    if ($group != $screen['group']) {
		if($group) {
		    $html .= "\t\t</div>\n";
		    $html .= "\t\t<div data-role=\"controlgroup\" data-type=\"vertical\" data-theme=\"b\">\n";
		}
		$group = $screen['group'];
	    }
	    if($key != 'default') {
		$html .= sprintf("\t\t\t<a href=\"/%s/\" class=\"ui-btn ui-corner-all ui-icon-%s ui-btn-icon-left\">%s</a>\n", $key, $screen['icon'], $screen['name']);
	    }
	}
	$html .= "\t\t</div>\n";
	$html .= "\t\t<form method=\"post\" action=\"/\">\n";
	$html .= "\t\t\t<div data-role=\"controlgroup\" data-type=\"vertical\" data-theme=\"b\">\n";
	$html .= "\t\t\t\t<button name=\"action\" value=\"user-logout\" class=\"ui-btn ui-corner-all ui-icon-delete ui-btn-icon-left\">Kilépés</button>\n";
	$html .= "\t\t\t</div>\n";
	$html .= "\t\t</form>\n";
	$html .= "\t</div>\n";
	$html = preg_replace('/\t/', '    ', $html);
	return $html;
    }

    function homeScreenItems($screen) {
	global $conf;
	$html  = sprintf("\t<div data-role=\"header\" data-theme=\"a\">\n\t\t<h1>%s</h1>\n\t\t<a href=\"/\" data-icon=\"home\" data-theme=\"c\">vissza</a>\n\t</div>\n", $conf['screen'][$screen]['name']);
	$html .= "\t<div data-role=\"content\">\n";
	if(array_key_exists('items', $conf['screen'][$screen])) {
	    foreach($conf['screen'][$screen]['items'] as $key => $item) {
		switch($item['type']) {
		    case 'heading':
			$html .= sprintf("\t\t<div id=\"item%s\">\n%s\t\t</div>\n", $key, homeScreenHeading($item));
			break;
		    case 'text':
			$html .= sprintf("\t\t<div id=\"item%s\">\n%s\t\t</div>\n", $key, homeScreenText($item));
			break;
		    case 'value':
			$html .= sprintf("\t\t<div id=\"item%s\">\n%s\t\t</div>\n", $key, homeScreenValue($item));
			break;
		    case 'image':
			$html .= sprintf("\t\t<div id=\"item%s\">\n%s\t\t</div>\n", $key, homeScreenImage($screen, $key, $item));
			break;
		    case 'button':
			$html .= sprintf("\t\t<div id=\"item%s\">\n%s\t\t</div>\n", $key, homeScreenButton($screen, $key, $item));
			break;
		    case 'radio':
			$html .= sprintf("\t\t<div id=\"item%s\">\n%s\t\t</div>\n", $key, homeScreenRadio($screen, $key, $item));
			break;
		    case 'slider':
			$html .= sprintf("\t\t<div id=\"item%s\">\n%s\t\t</div>\n", $key, homeScreenSlider($screen, $key, $item));
			break;
		    case 'range':
			$html .= sprintf("\t\t<div id=\"item%s\">\n%s\t\t</div>\n", $key, homeScreenRange($screen, $key, $item));
			break;
		    case 'status':
			$html .= homeScreenStatus();
			break;
		    case 'permission':
			$html .= homeScreenPermission();
			break;
		    default:
			//
		}
	    }
	}
	$html .= "\t</div>\n";
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenLogin() {
	global $conf;
	$html  = sprintf("\t<div data-role=\"header\">\n\t\t<h1>%s</h1>\n\t</div>\n", $conf['system']['name']);
	$html .= "\t<div data-role=\"content\">\n";
	$html .= "\t\t<div data-role=\"collapsible\" data-collapsed=\"false\" data-theme=\"b\" data-content-theme=\"a\" data-expanded-icon=\"star\" data-collapsed-icon=\"star\">\n";
	$html .= "\t\t\t<h1>Belépés</h1>\n";
	$html .= sprintf("\t\t\t<form action=\"%s\" method=\"post\">\n", $_SERVER['REQUEST_URI']);
	if($_GET['username']) {
	    $html .= "\t\t\t\t<h3 style=\"color:red;\">Hibás felhasználó vagy jelszó!</h3>\n";
	}
	$html .= "\t\t\t\t<input type=\"hidden\" name=\"action\" value=\"user-login\">\n";
	$html .= "\t\t\t\t<input type=\"text\" name=\"username\" value=\"\" placeholder=\"Felhasználó\"/>\n";
	$html .= "\t\t\t\t<input type=\"password\" name=\"password\" value=\"\" placeholder=\"Jelszó\"/>\n";
	$html .= sprintf("\t\t\t\t<input type=\"submit\" value=\"Belépés\" data-icon=\"check\" data-inline=\"true\" />\n");
	$html .= "\t\t\t</form>\n";
	$html .= "\t\t</div>\n\t</div>\n";
	$html = preg_replace('/\t/', '    ', $html);
	return $html;
    }

    function homeScreenText($item) {
	global $conf;
	$html = sprintf("\t\t\t<p>%s</p>\n", $item['exp']);
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenHeading($item) {
	global $conf;
	if(array_key_exists('level', $item)) {
	    $level = $item['level'];
	} else {
	    $level = 3;
	}
	$html = sprintf("\t\t\t<h%s>%s</h%s>\n", $level, $item['exp'], $level);
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenValue($item) {
	global $conf;
	$html .= "\t\t\t<ul data-role=\"listview\" data-inset=\"true\">\n";
	if($item['name']) {
	    if($item['info']) {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $item['name'], homeExpression($item['info']));
	    } else {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s</li>\n", $item['name']);
	    }
	}
	if($item['subtype'] == 'horizontal') {
	    if(sizeof($item['label']) == 1) {
		$html .= sprintf("\t\t\t\t<li style=\"text-align:center;\" data-theme=\"c\">%s</li>\n", $item['label']);
		$html .= sprintf("\t\t\t\t<li style=\"text-align:center;\" data-theme=\"d\">%s</li>\n", homeExpression($item['exp']));
	    } else {
		$html .= sprintf("\t\t\t\t<li>\n\t\t\t\t\t<div class=\"ui-grid-%s\">\n", chr(95+sizeof($item['label'])));
		$i=0;
		foreach($item['label'] as $key => $subitem) {
		    $html .= sprintf("\t\t\t\t\t\t<div class=\"ui-block-%s\" style=\"text-align:center;\">%s</div>\n", chr(97+$i), $item['label'][$key]);
		    $i++;
		}
		$html .= "\t\t\t\t\t</div>\n\t\t\t\t</li>\n\t\t\t\t<li>\n";
		$html .= sprintf("\t\t\t\t\t<div class=\"ui-grid-%s\">\n", chr(95+sizeof($item['label'])));
		$i=0;
		foreach($item['exp'] as $key => $subitem) {
		    $html .= sprintf("\t\t\t\t\t\t<div class=\"ui-block-%s\" style=\"text-align:center;\">%s</div>\n", chr(97+$i), homeExpression($item['exp'][$key]));
		    $i++;
		}
		$html .= "\t\t\t\t\t</div>\n\t\t\t\t</li>\n";
	    }
	} else {
	    if(sizeof($item['label']) == 1) {
		$html .= sprintf("\t\t\t\t<li data-theme=\"d\">%s<span class=\"ui-li-aside\">%s</span></li>\n", $item['label'], homeExpression($item['exp']));
	    } else {
		foreach($item['label'] as $key => $subitem) {
		    $html .= sprintf("\t\t\t\t<li data-theme=\"d\">%s<span class=\"ui-li-aside\">%s</span></li>\n", $item['label'][$key], homeExpression($item['exp'][$key]));
		}
	    }
	}
	if($item['desc']) {
	    $html .= sprintf("\t\t\t\t<li><small>%s</small></li>\n", homeExpression($item['desc']));
	}
	$html .= "\t\t\t</ul>\n";
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenImage($screen, $key, $item) {
	$rand = rand(100000,999999);
	$html .= "\t\t\t<ul data-role=\"listview\" data-inset=\"true\">\n";
	if($item['name']) {
	    if($item['info']) {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $item['name'], homeExpression($item['info']));
	    } else {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s</li>\n", $item['name']);
	    }
	}
	$html .= sprintf(
	    "\t\t\t\t<li style=\"padding:0px; border:0px;\"><p style=\"margin:0px;\"><img id=\"item%simage\" src=\"/include/allcontrol/image.php?screen=%s&amp;item=%s&amp;rand=%s\" style=\"margin-bottom:-4px; width:100%%;\" alt=\"%s\" /></p></li>\n",
	    $key, $screen, $key, $rand, $item['name']
	);
	if($item['desc']) {
	    $html .= sprintf("\t\t\t\t<li><small>%s</small></li>\n", homeExpression($item['desc']));
	}
	$html .= "\t\t\t</ul>\n";
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenButton($screen, $key, $item) {
	$html .= "\t\t\t<ul data-role=\"listview\" data-inset=\"true\">\n";
	if($item['name']) {
	    if($item['info']) {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $item['name'], homeExpression($item['info']));
	    } else {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s</li>\n", $item['name']);
	    }
	}
	$html .= sprintf("\t\t\t\t<li data-theme=\"d\">\n\t\t\t\t\t<form action=\"\" method=\"post\" id=\"item%sform\">\n", $key);
	$html .= "\t\t\t\t\t\t<input type=\"hidden\" name=\"action\" value=\"value\">\n";
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"screen\" value=\"%s\">\n", $screen);
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"key\" value=\"%s\">\n", $key);
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"value\" value=\"\" id=\"item%sformvalue\">\n", $key);
	$html .= sprintf("\t\t\t\t\t\t<fieldset data-role=\"controlgroup\" data-type=\"%s\">\n", $item['subtype']);
	foreach($item['items'] as $subkey => $subitem) {
	    $html .= sprintf("\t\t\t\t\t\t\t<input data-inset=\"false\" type=\"button\" id=\"item%sbutton%s\" value=\"%s\" data-theme=\"a\"/>\n", $key, $subkey, $subitem['name']);
	}
	$html .= "\t\t\t\t\t\t</fieldset>\n\t\t\t\t\t</form>\n\t\t\t\t</li>\n";
	if($item['desc']) {
	    $html .= sprintf("\t\t\t\t<li data-theme=\"c\"><small>%s</small></li>\n", homeExpression($item['desc']));
	}
	$html .= "\t\t\t</ul>\n";
	$html .= "\t\t\t<script>\n";
	foreach($item['items'] as $subkey => $subitem) {
	    $html .= sprintf("\t\t\t\t\$('#item%sbutton%s').on('click', function(){\$('#item%sformvalue').val('%s'); \$('#item%sform').submit()});\n", $key, $subkey, $key, $subkey, $key, $key);
	}
	$html .= "\t\t\t</script>\n";
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenRadio($screen, $key, $item) {
	$html .= "\t\t\t<ul data-role=\"listview\" data-inset=\"true\">\n";
	if($item['name']) {
	    if($item['info']) {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $item['name'], homeExpression($item['info']));
	    } else {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s</li>\n", $item['name']);
	    }
	}
	$html .= sprintf("\t\t\t\t<li data-theme=\"d\">\n\t\t\t\t\t<form action=\"\" method=\"post\" id=\"item%sform\">\n", $key);
	$html .= "\t\t\t\t\t\t<input type=\"hidden\" name=\"action\" value=\"value\">\n";
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"screen\" value=\"%s\">\n", $screen);
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"key\" value=\"%s\">\n", $key);
	$html .= sprintf("\t\t\t\t\t\t<fieldset data-role=\"controlgroup\" data-type=\"%s\">\n", $item['subtype']);
	foreach($item['items'] as $subkey => $subitem) {
	    if(homeExpression($item['exp']) == $subkey) {
		$checked = " checked=\"checked\"";
		$theme   = "a";
	    } else {
		$checked = "";
		$theme   = "c";
	    }
	    $html .= sprintf("\t\t\t\t\t\t\t<label for=\"item%sradio%s\">%s</label>\n", $key, $subkey, $subitem['name']);
	    $html .= sprintf("\t\t\t\t\t\t\t<input data-inset=\"false\" type=\"radio\" name=\"value\" id=\"item%sradio%s\" value=\"%s\" data-theme=\"%s\"%s/>\n", $key, $subkey, $subkey, $theme, $checked);
	}
	$html .= "\t\t\t\t\t\t</fieldset>\n\t\t\t\t\t</form>\n\t\t\t\t</li>\n";
	if($item['desc']) {
	    $html .= sprintf("\t\t\t\t<li data-theme=\"c\"><small>%s</small></li>\n", homeExpression($item['desc']));
	}
	$html .= "\t\t\t</ul>\n";
	$html .= "\t\t\t<script>\n";
	foreach($item['items'] as $subkey => $subitem) {
	    $html .= sprintf("\t\t\t\t\$('#item%sradio%s').on('change', function(){\$('#item%sform').submit()});\n", $key, $subkey, $key);
	}
	$html .= "\t\t\t</script>\n";
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenSlider($screen, $key, $item) {
	$html .= "\t\t\t<ul data-role=\"listview\" data-inset=\"true\">\n";
	if($item['name']) {
	    if($item['info']) {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $item['name'], homeExpression($item['info']));
	    } else {
		$html .= sprintf("\t\t\t\t<li data-theme=\"b\" data-role=\"list-divider\">%s</li>\n", $item['name']);
	    }
	}
	$html .= sprintf("\t\t\t\t<li data-theme=\"d\">\n\t\t\t\t\t<form action=\"\" method=\"post\" id=\"item%sform\">\n", $key);
	$html .=         "\t\t\t\t\t\t<input type=\"hidden\" name=\"action\" value=\"value\">\n";
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"screen\" value=\"%s\">\n", $screen);
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"key\" value=\"%s\">\n", $key);
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"subkey\" value=\"\" id=\"item%sformsubkey\">\n", $key);
	$html .= sprintf("\t\t\t\t\t\t<input type=\"hidden\" name=\"value\" value=\"\" id=\"item%sformvalue\">\n", $key);

	foreach($item['items'] as $subkey => $subitem) {
	    $html .= sprintf("\t\t\t\t\t\t<label for=\"slider%s\">%s</label>\n", $subkey, $subitem['name']);
	    $html .= sprintf("\t\t\t\t\t\t<input type=\"range\" name=\"slider%s\" id=\"item%sslider%s\" value=\"%s\" min=\"%s\" max=\"%s\" data-highlight=\"true\"/>\n", $subkey, $key, $subkey, homeExpression($subitem['exp']), homeExpression($subitem['min']), homeExpression($subitem['max']));
	}
	$html .= "\t\t\t\t\t</form>\n\t\t\t\t</li>\n";
	if($item['desc']) {
	    $html .= sprintf("\t\t\t\t<li data-theme=\"c\"><small>%s</small></li>\n", homeExpression($item['desc']));
	}
	$html .= "\t\t\t</ul>\n";
	$html .= "\t\t\t<script>\n";
	$html .= "\t\t\t\tsetTimeout(function(){\n";
	foreach($item['items'] as $subkey => $subitem) {
	    $html .= sprintf("\t\t\t\t\t\$('#item%sslider%s').on('slidestop', function(){\$('#item%sformsubkey').val('%s'); \$('#item%sformvalue').val($('#item%sslider%s').val()); \$('#item%sform').submit()});\n", $key, $subkey, $key, $subkey, $key, $key, $subkey, $key);
	}
	$html .= "\t\t\t\t}, 1000);\n";
	$html .= "\t\t\t</script>\n";
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenStatus() {
	global $conf;
	$html  = '';
	$html .= "\t\t<div data-role=\"collapsible-set\" data-inset=\"true\">\n";

	$html .= "\t\t\t<div data-role=\"collapsible\" data-collapsed=\"false\" data-theme=\"b\" data-content-theme=\"d\">\n\t\t\t\t<h3>Sensors</h3>\n";
	foreach($conf['sensor'] as $key => $sensor) {
	    $html .= "\t\t\t\t<ul data-role=\"listview\" data-inset=\"false\">\n";
	    $html .= sprintf("\t\t\t\t\t<li data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $key, homeSensorGetValue($key));
	    $html .= sprintf("\t\t\t\t\t<li>\n");
	    $html .= sprintf("\t\t\t\t\t\t<p>Type / subtype: %s / %s</p>\n", $sensor['type'], $sensor['subtype']);
	    $html .= sprintf("\t\t\t\t\t\t<p>Default value: %s</p>\n", $sensor['default']);
	    $user = homeSensorGetUpdateUser($key);
	    if($user === false) {
		$user = 'system';
	    }
	    $html .= sprintf("\t\t\t\t\t\t<p>Update user: %s</p>\n", $user);
	    $ts = date("Y-m-d H:i:s", homeSensorGetUpdateTS($key));
	    if(substr($ts,0,10) == '1970-01-01') {
		$ts = 'never';
	    }
	    $html .= sprintf("\t\t\t\t\t\t<p>Update time: %s</p>\n", $ts);
	    $html .= sprintf("\t\t\t\t\t</li>\n");
	    $html .= "\t\t\t\t</ul>\n";
	}
	$html .= "\t\t\t</div>\n";

	$html .= "\t\t\t<div data-role=\"collapsible\" data-collapsed=\"true\" data-theme=\"b\" data-content-theme=\"d\">\n\t\t\t\t<h3>Inputs</h3>\n";
	foreach($conf['input'] as $key => $input) {
	    $html .= "\t\t\t\t<ul data-role=\"listview\" data-inset=\"false\">\n";
	    $html .= sprintf("\t\t\t\t\t<li data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $key, homeInputGetValue($key));
	    $html .= sprintf("\t\t\t\t\t<li>\n");
	    $html .= sprintf("\t\t\t\t\t\t<p>Default value: %s</p>\n", $input['default']);
	    $user = homeInputGetUpdateUser($key);
	    if($user === false) {
		$user = 'system';
	    }
	    $html .= sprintf("\t\t\t\t\t\t<p>Update user: %s</p>\n", $user);
	    $ts = date("Y-m-d H:i:s", homeInputGetUpdateTS($key));
	    if(substr($ts,0,10) == '1970-01-01') {
		$ts = 'never';
	    }
	    $html .= sprintf("\t\t\t\t\t\t<p>Update time: %s</p>\n", $ts);
	    $html .= sprintf("\t\t\t\t\t</li>\n");
	    $html .= "\t\t\t\t</ul>\n";
	}
	$html .= "\t\t\t</div>\n";

	$html .= "\t\t\t<div data-role=\"collapsible\" data-collapsed=\"true\" data-theme=\"b\" data-content-theme=\"d\">\n\t\t\t\t<h3>Variables</h3>\n";
	foreach($conf['variable'] as $key => $variable) {
	    $html .= "\t\t\t\t<ul data-role=\"listview\" data-inset=\"false\">\n";
	    $html .= sprintf("\t\t\t\t\t<li data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $key, homeVariableGetValue($key));
	    $html .= sprintf("\t\t\t\t\t<li>\n");
	    $html .= sprintf("\t\t\t\t\t\t<p>Expression: %s</p>\n", $variable['exp']);
	    $html .= sprintf("\t\t\t\t\t\t<p>Update user: %s</p>\n", homeVariableGetUpdateUser($key));
	    $html .= sprintf("\t\t\t\t\t\t<p>Update time: %s</p>\n", date("Y-m-d H:i:s", homeVariableGetUpdateTS($key)));
	    $html .= sprintf("\t\t\t\t\t</li>\n");
	    $html .= "\t\t\t\t</ul>\n";
	}
	$html .= "\t\t\t</div>\n";

	$html .= "\t\t\t<div data-role=\"collapsible\" data-collapsed=\"true\" data-theme=\"b\" data-content-theme=\"d\">\n\t\t\t\t<h3>Outputs</h3>\n";
	foreach($conf['output'] as $key => $output) {
	    $html .= "\t\t\t\t<ul data-role=\"listview\" data-inset=\"false\">\n";
	    $html .= sprintf("\t\t\t\t\t<li data-role=\"list-divider\">%s<span class=\"ui-li-count\">%s</span></li>\n", $key, homeOutputGetValue($key));
	    $html .= sprintf("\t\t\t\t\t<li>\n");
	    $html .= sprintf("\t\t\t\t\t\t<p>Type / subtype: %s / %s</p>\n", $output['type'], $output['subtype']);
	    $html .= sprintf("\t\t\t\t\t\t<p>Expression: %s</p>\n", $output['exp']);
	    $html .= sprintf("\t\t\t\t\t\t<p>Update user: %s</p>\n", homeOutputGetUpdateUser($key));
	    $html .= sprintf("\t\t\t\t\t\t<p>Update time: %s</p>\n", date("Y-m-d H:i:s", homeOutputGetUpdateTS($key)));
	    $html .= sprintf("\t\t\t\t\t</li>\n");
	    $html .= "\t\t\t\t</ul>\n";
	}
	$html .= "\t\t\t</div>\n";

	$html .= "\t\t</div>\n";
	return preg_replace('/\t/', '    ', $html);
    }

    function homeScreenPermission() {
	global $user;
	$html  = '';
	$html .= "\t\t<div data-role=\"collapsible-set\" data-inset=\"true\">\n";

	$html .= "\t\t\t<div data-role=\"collapsible\" data-collapsed=\"false\" data-theme=\"b\" data-content-theme=\"d\">\n\t\t\t\t<h3>Users</h3>\n";
	foreach($user as $key => $value) {
	    $html .= "\t\t\t\t<ul data-role=\"listview\" data-inset=\"false\">\n";
	    $html .= sprintf("\t\t\t\t\t<li data-role=\"list-divider\">%s</li>\n", $key);
	    $html .= sprintf("\t\t\t\t\t<li>\n");
	    if(array_key_exists('group', $value)) {
		$html .= sprintf("\t\t\t\t\t\t<p>Group: %s</p>\n", $value['group']);
	    }
//	    $html .= sprintf("\t\t\t\t\t\t<p>PW: %s</p>\n", $value['password']);
	    $html .= sprintf("\t\t\t\t\t</li>\n");
	    $html .= "\t\t\t\t</ul>\n";
	}
	$html .= "\t\t\t</div>\n";

//	$html .= "\t\t\t<div data-role=\"collapsible\" data-collapsed=\"true\" data-theme=\"b\" data-content-theme=\"d\">\n\t\t\t\t<h3>Groups</h3>\n";
//	foreach($user as $key => $value) {
//	    $html .= "\t\t\t\t<ul data-role=\"listview\" data-inset=\"false\">\n";
//	    $html .= sprintf("\t\t\t\t\t<li data-role=\"list-divider\">%s</li>\n", $key);
//	    $html .= sprintf("\t\t\t\t\t<li>\n");
//	    if(array_key_exists('group', $value)) {
//		$html .= sprintf("\t\t\t\t\t\t<p>Group: %s</p>\n", $value['group']);
//	    }
//	    $html .= sprintf("\t\t\t\t\t</li>\n");
//	    $html .= "\t\t\t\t</ul>\n";
//	}
//	$html .= "\t\t\t</div>\n";


	$html .= "\t\t</div>\n";
	return preg_replace('/\t/', '    ', $html);
    }

?>