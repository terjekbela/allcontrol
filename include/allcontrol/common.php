<?php
    global $conf, $cache;

    $pwd = '/var/www';
    require_once($pwd.'/local/config.php');
    require_once($pwd.'/local/user.php');
    require_once($pwd.'/include/allcontrol/sensor.php');
    require_once($pwd.'/include/allcontrol/input.php');
    require_once($pwd.'/include/allcontrol/variable.php');
    require_once($pwd.'/include/allcontrol/output.php');
    require_once($pwd.'/include/allcontrol/screen.php');
//    require_once 'HTTP/Request.php';

    setlocale(LC_ALL, $conf['system']['locale']);
    date_default_timezone_set($conf['system']['timezone']);
    error_reporting(E_ALL ^ E_NOTICE);

    $cache = homeCacheConnect();

    function homeCacheConnect() {
	global $conf, $cache;
	switch($conf['system']['cache']['type']) {
	    case 'memcache':
		$cache = new Memcached();
		$cache-> addServer($conf['system']['cache']['memcache']['host'], $conf['system']['cache']['memcache']['port']);
		break;
	    case 'cassandra':
		//
		break;
	    default:
		//
	}
	return $cache;
    }

    function homeCacheGet($table, $field, $key) {
	global $conf, $cache;
	$value = '';
	switch($conf['system']['cache']['type']) {
	    case 'memcache':
		$hash  = sprintf("%s-%s-%s-%s", $conf['system']['uid'], $table, $key, $field);
		$value = $cache->get($hash);
		break;
	    default:
		//
	}
	return $value;
    }

    function homeCacheSet($table, $field, $key, $value) {
	global $conf, $cache;
	switch($conf['system']['cache']['type']) {
	    case 'memcache':
		$hash  = sprintf("%s-%s-%s-%s", $conf['system']['uid'], $table, $key, $field);
		$value = $cache->set($hash, $value);
		break;
	    default:
		//
	}
	return $value;
    }

    function homeCacheDelete($table, $field, $key) {
	global $conf, $cache;
	switch($conf['system']['cache']['type']) {
	    case 'memcache':
		$hash  = sprintf("%s-%s-%s-%s", $conf['system']['uid'], $table, $key, $field);
		$value = $cache->delete($hash);
		break;
	    default:
		//
	}
	return $value;
    }

    function homeCacheArch($category, $key, $value) {
        global $conf;
        if(array_key_exists('arch-freq', $conf[$category][$key]) && is_numeric($conf[$category][$key]['arch-freq'])) {
            $freq  = $conf[$category][$key]['arch-freq'];
            $items = $conf[$category][$key]['arch-items'];
            $json  = homeCacheGet($category, 'arch', $key);
            if(homeCacheFound()) {
                $arch = json_decode($json, true);
            } else {
                $arch = array();
            }
            $date = date('U');
            foreach($arch as $archTS => $archValue) {
                if($archTS < $date - $conf[$category][$key]['arch-freq'] * $conf[$category][$key]['arch-items']) {
                    unset($arch[$archTS]);
                }
            }
            $ts = $date - $date % $freq;
            $arch[$ts] = $value;
            $json = json_encode($arch);
            homeCacheSet($category, 'arch', $key, $json);
        }
    }

    function homeCacheFound() {
	global $conf, $cache;
	$found = false;
	switch($conf['system']['cache']['type']) {
	    case 'memcache':
		if($cache->getResultCode() == Memcached::RES_NOTFOUND) {
		    $found = false;
		} else {
		    $found = true;
		}
	    case 'cassandra':
		//
		break;
	    default:
		//
	}
	return $found;
    }

    function homeExpression($exp) {
	global $conf;
	if(strpos($exp, '$')===false) {
	    return $exp;
	} else {
	    foreach($conf['sensor'] as $key => $sensor) {
		$s[$key]    = homeSensorGetValue($key);
		$sUU[$key]  = homeSensorGetUpdateUser($key);
		$vUTS[$key] = date('Y-m-d H:i', homeSensorGetUpdateTS($key));
	    }
	    foreach($conf['input'] as $key => $input) {
		$i[$key]    = homeInputGetValue($key);
		$iUU[$key]  = homeInputGetUpdateUser($key);
		$iUTS[$key] = date('Y-m-d H:i', homeInputGetUpdateTS($key));
	    }
	    foreach($conf['variable'] as $key => $variable) {
		$v[$key]    = homeVariableGetValue($key);
		$vUU[$key]  = homeVariableGetUpdateUser($key);
		$vUTS[$key] = date('Y-m-d H:i', homeVariableGetUpdateTS($key));
	    }
	    $exp = sprintf('$value = %s;', $exp);
	    error_reporting(0);
	    $success = eval($exp);
	    error_reporting(E_ALL ^ E_NOTICE);
	    if($success === false) {
		return false;
	    } else {
		return $value;
	    }
	}
    }
?>