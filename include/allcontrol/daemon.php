#!/usr/bin/php
<?php
    global $conf, $cache;

    $pwd = '/var/www';
    require_once($pwd.'/include/allcontrol/common.php');

    if(array_key_exists('sensor', $conf) && sizeof($conf['sensor'])) {
	foreach($conf['sensor'] as $key => $sensor) {
	    $value = homeSensorReadValue($key);
	    if($value === false) {
		homeSensorDeleteValue($key);
	    } else {
		homeSensorSetValue($key, $value);
	    }
	}
    }

    if(array_key_exists('variable', $conf) && sizeof($conf['variable'])) {
	foreach($conf['variable'] as $key => $variable) {
	    $value = homeExpression($variable['exp']);
	    if($value === false) {
		homeVariableDeleteValue($key);
	    } else {
		homeVariableSetValue($key, $value);
	    }
	}
    }

    if(array_key_exists('output', $conf) && sizeof($conf['output'])) {
	foreach($conf['output'] as $key => $output) {
	    $value = homeExpression($output['exp']);
	    if($value === false) {
		homeOutputDeleteValue($key);
	    } else {
		homeOutputSetValue($key, $value);
		homeOutputWriteValue($key, $value);
	    }
	}
    }


?>
