<?php
    function homeOutputGetValue($key) {
        global $conf, $cache;
        $value = homeCacheGet('output', 'value', $key);
        if(homeCacheFound()) {
            return $value;
        } else {
            return $conf['output'][$key]['default'];
        }
    }

    function homeOutputGetUpdateUser($key) {
        global $conf, $cache;
        $value = homeCacheGet('output', 'update-user', $key);
        if(homeCacheFound()) {
            return $value;
        } else {
            return $conf['output'][$key]['default'];
        }
    }

    function homeOutputGetUpdateTS($key) {
        global $conf, $cache;
        $value = homeCacheGet('output', 'update-ts', $key);
        if(homeCacheFound()) {
            return $value;
        } else {
            return $conf['output'][$key]['default'];
        }
    }

    function homeOutputGetArch($key) {
	global $conf, $cache;
	$json = homeCacheGet('output', 'arch', $key);
	if(homeCacheFound()) {
	    return json_decode($json, true);
	} else {
	    return array();
	}
    }

    function homeOutputSetValue($key, $value) {
	global $conf, $cache;
	homeCacheArch('output',                $key, $value   );
	homeCacheSet ('output', 'update-user', $key, 'system' );
	homeCacheSet ('output', 'update-ts',   $key, date('U'));
	return  homeCacheSet ('output', 'value',       $key, $value   );
    }

    function homeOutputWriteValue($key, $value) {
        global $conf, $cache;
        $file  = $conf['output'][$key]['file'];
        if(file_exists($file)) {
            if(file_put_contents($file, $value) === false) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    function homeOutputDeleteValue($key) {
        global $conf, $cache;
                homeCacheDelete('output', 'update-user', $key);
                homeCacheDelete('output', 'update-ts',   $key);
        return  homeCacheDelete('output', 'value',       $key);
    }
?>