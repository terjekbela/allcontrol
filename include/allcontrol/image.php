<?php
    global $conf;
    require_once('common.php');

    $paramScreen  = $_GET['screen']  ? $_GET['screen']  : '';
    $paramItem    = $_GET['item']    ? $_GET['item']    : 0;
    $paramWidth   = $_GET['width']   ? $_GET['width']   : 768;
    $paramHeight  = $_GET['height']  ? $_GET['height']  : 200;

    header('Content-Type: image/png');
    $image = imagecreatetruecolor($paramWidth, $paramHeight);
    homeImageFilledRectangle($image, 0 , 0, $paramWidth, $paramHeight, 255, 255, 255 );
    foreach($conf['screen'][$paramScreen]['items'][$paramItem]['items'] as $key => $item) {
	switch($item['type']) {
	    case 'filledrectangle':
		homeImageFilledRectangle(
		    $image,
		    $item['x1'], $item['y1'], $item['x2'], $item['y2'],
		    $item['r'], $item['g'], $item['b']
		);
		break;
	    case 'graphline':
		homeImageGraphLine(
		    $image,
		    $item['x1'], $item['y1'], $item['x2'], $item['y2'],
		    $item['r'],  $item['g'],  $item['b'],
		    $item['atype'], $item['akey'],
		    $item['rxmin'], $item['rymin'], $item['rymax'], $item['padding'], $item['thickness']
		);
		break;
	    case 'graphbox':
		homeImageGraphBox(
		    $image,
		    $item['x1'], $item['y1'], $item['x2'], $item['y2'],
		    $item['r'],  $item['g'],  $item['b'],
		    $item['atype'], $item['akey'],
		    $item['rxmin'], $item['rymin'], $item['rymax']
		);
		break;
	    case 'xgrid':
		homeImageXGrid(
		    $image,
		    $item['x1'], $item['y1'], $item['x2'], $item['y2'],
		    $item['r'],  $item['g'],  $item['b'], $item['rxmin'], $item['thickness']);
		break;
	    case 'yline':
		homeImageYLine(
		    $image,
		    $item['x1'], $item['y1'], $item['x2'], $item['y2'],
		    $item['r'],  $item['g'],  $item['b'], $item['exp'], $item['thickness']);
		break;
	    default:
		//
	}
    }
    imagepng($image);
    imagedestroy($image);

    function homeImageFilledRectangle($image, $x1, $y1, $x2, $y2, $r, $g, $b) {
	$color = imagecolorallocate($image, $r, $g, $b);
	imagefilledrectangle($image, $x1, $y1, $x2, $y2, $color);
    }

    function homeImageXGrid($image, $x1, $y1, $x2, $y2, $r, $g, $b, $rxMin, $t = 1) {
	$color = imagecolorallocate($image, $r, $g, $b);
	imagesetthickness($image, $t);
	$x = 0;
	while ($x < $rxMin) {
	    $x += 1600;
	    $fromX = $x2 - 
	    imageline($image, $fromX, $fromY, $toX, $toY, $color);
	}
    }

    function homeImageYLine($image, $x1, $y1, $x2, $y2, $r, $g, $b, $exp, $t = 1) {
	$color = imagecolorallocate($image, $r, $g, $b);
	imagesetthickness($image, $t);
	$aValue = homeExpression($exp);
	$fromX = $x1;
	$fromY = $y2 - ($aValue - $ryMin) / ($ryMax - $ryMin) * ($y2 - $y1);
	$toX   = $x2;
	$toY   = $y2 - ($aValue - $ryMin) / ($ryMax - $ryMin) * ($y2 - $y1);
	imageline($image, $fromX, $fromY, $toX, $toY, $color);
    }

    function homeImageGraphLine($image, $x1, $y1, $x2, $y2, $r, $g, $b, $aType, $aKey, $rxMin, $ryMin, $ryMax, $padding = 10, $t = 1) {
	global $conf;
	switch($aType) {
	    case 'sensor':
		$archTemp = homeSensorGetArch($aKey);
		break;
	    case 'variable':
		$archTemp = homeVariableGetArch($aKey);
		break;
	    case 'output':
		$archTemp = homeOutputGetArch($aKey);
		break;
	    default:
		//
	}
	$date     = date('U') - date('U') % $conf[$aType][$aKey]['arch-freq'];
	$fromX    = false;
	$fromY    = false;
	$color    = imagecolorallocate($image, $r, $g, $b);
	imagesetthickness($image, $t);
	foreach($archTemp as $aTS => $aValue) {
	    if($aTS >= $date - $rxMin) {
		$arch[$aTS] = $aValue;
	    }
	}
	if($ryMin === 'auto') {
	    $ryMin = min($arch);
	}
	if($ryMax === 'auto') {
	    $ryMax = max($arch);
	}
	$ryMin = $ryMin - ($ryMax - $ryMin) / 100 * $padding;
	foreach($arch as $aTS => $aValue) {
		$toX = round($x2 - ($date - $aTS) / $rxMin * ($x2-$x1));
		$toY = $y2 - ($aValue - $ryMin) / ($ryMax - $ryMin) * ($y2 - $y1);
		if($fromX === false && $fromY === false) {
		    $fromX = $toX;
		    $fromY = $toY;
		} else {
		    if(isset($toY)) {
			if($fromY >= $y1 && $fromY <= $y2 && $toY >= $y1 && $toY <= $y2){
			    imageline($image, $fromX, $fromY, $toX, $toY, $color);
			}
			$fromX = $toX;
			$fromY = $toY;
		    }
		}
	}
    }

    function homeImageGraphBox($image, $x1, $y1, $x2, $y2, $r, $g, $b, $aType, $aKey, $rxMin, $ryMin, $ryMax) {
	global $conf;
	switch($aType) {
	    case 'sensor':
		$arch = homeSensorGetArch($aKey);
		break;
	    case 'variable':
		$arch = homeVariableGetArch($aKey);
		break;
	    case 'output':
		$arch = homeOutputGetArch($aKey);
		break;
	    default:
		//
	}
	$date     = date('U') - date('U') % $conf[$aType][$aKey]['arch-freq'];
	$fromX    = false;
	$color    = imagecolorallocate($image, $r, $g, $b);
	foreach($arch as $aTS => $aValue) {
	    if($aTS >= $date - $rxMin) {
		$toX = round($x2 - ($date - $aTS) / $rxMin * ($x2-$x1));
		if($fromX === false) {
		    $fromX = $toX;
		} else {
		    if($aValue != 0) {
		        imagefilledrectangle($image, $fromX, $y1, $toX, $y2, $color);
		    }
		    $fromX = $toX;
		}
	    }
	}
    }
?>