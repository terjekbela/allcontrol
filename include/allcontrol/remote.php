<?php
    global $conf;
    require_once('common.php');

    $paramAction   = $_GET['action']   ? $_GET['action']   : 'read';
    $paramCategory = $_GET['category'] ? $_GET['category'] : 'sensor';
    $paramKey      = $_GET['key']      ? $_GET['key']      : '';

    switch($paramAction) {
	case 'read':
	    switch($paramCategory) {
		case 'sensor':
		    $json['value']       = homeSensorGetValue($paramKey);
		    $json['update-user'] = homeSensorGetUpdateUser($paramKey);
		    $json['update-ts']   = homeSensorGetUpdateTS($paramKey);
		    $json['status']      = true;
		    break;
		case 'input':
		    break;
		case 'variable':
		    break;
		default:
		    $json['status'] = false;
	    }
	    break;
	case 'write':
	    //
	    break;
	default:
	    $json['status'] = false;
    }
    print json_encode($json);
?>