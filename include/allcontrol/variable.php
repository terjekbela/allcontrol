<?php

    function homeVariableGetValue($key) {
	global $conf;
	$value = homeCacheGet('variable', 'value', $key);
	if(homeCacheFound()) {
	    return $value;
	} else {
	    return $conf['variable'][$key]['default'];
	}
    }

    function homeVariableGetUpdateUser($key) {
        global $conf;
        $value = homeCacheGet('variable', 'update-user', $key);
        if(homeCacheFound()) {
            return $value;
        } else {
            return false;
        }
    }

    function homeVariableGetUpdateTS($key) {
        global $conf;
        $value = homeCacheGet('variable', 'update-ts', $key);
        if(homeCacheFound()) {
            return $value;
        } else {
            return false;
        }
    }

    function homeVariableGetArch($key) {
        global $conf;
        $json = homeCacheGet('variable', 'arch', $key);
        if(homeCacheFound()) {
            return json_decode($json, true);
        } else {
            return array();
        }
    }

    function homeVariableSetValue($key, $value) {
	global $conf;
		homeCacheArch('variable',                $key, $value   );
		homeCacheSet ('variable', 'update-user', $key, 'system' );
                homeCacheSet ('variable', 'update-ts',   $key, date('U'));
	return  homeCacheSet ('variable', 'value',       $key, $value   );
    }

    function homeVariableDeleteValue($key) {
	global $conf;
	return homeCacheDelete('variable', 'value', $key);
    }

?>