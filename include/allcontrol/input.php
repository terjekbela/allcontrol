<?php

    function homeInputGetValue($key) {
	global $conf, $cache;
	$value = homeCacheGet('input', 'value', $key);
	if(homeCacheFound()) {
	    return $value;
	} else {
	    return $conf['input'][$key]['default'];
	}
    }

    function homeInputGetUpdateUser($key) {
	global $conf, $cache;
	$value = homeCacheGet('input', 'update-user', $key);
	if(homeCacheFound()) {
	    return $value;
	} else {
	    return false;
	}
    }

    function homeInputGetUpdateTS($key) {
	global $conf, $cache;
	$value = homeCacheGet('input', 'update-ts', $key);
	if(homeCacheFound()) {
	    return $value;
	} else {
	    return false;
	}
    }

    function homeInputSetValue($key, $value) {
	global $conf, $cache;
		homeCacheArch('input',                 $key, $value   );
		homeCacheSet ('input', 'update-user',  $key, $_SESSION['username'] );
		homeCacheSet ('input', 'update-ts',    $key, date('U'));
	return  homeCacheSet ('input', 'value',        $key, $value   );
    }

?>