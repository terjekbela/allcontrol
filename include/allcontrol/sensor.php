<?php
    function homeSensorReadValue($key) {
	global $conf;
	$value = false;
	$sensor = $conf['sensor'][$key];
	switch($sensor['type']) {
	    case '1wire':
		switch($sensor['subtype']) {
		    case 'thermo':
			$file = $sensor['file'];
			if(file_exists($file)) {
			    $value = trim(file_get_contents($file));
			} else {
			    $value = false;
			}
			break;
		    case 'humidity':
			$file = $sensor['file'];
			if(file_exists($file)) {
			    $value = trim(file_get_contents($file));
			} else {
			    $value = false;
			}
			break;
		    default:
			$value = false;
		}
		break;
	    case 'linux':
		$file = '/proc/loadavg';
		if(file_exists($file)) {
		    $raw = trim(file_get_contents($file));
		    switch($sensor['subtype']) {
			case 'load':
			    $arr = explode(' ', $raw);
			    $value = $arr[0];
			    break;
			case 'load5':
			    $arr = explode(' ', $raw);
			    $value = $arr[1];
			    break;
			case 'load15':
			    $arr = explode(' ', $raw);
			    $value = $arr[2];
			    break;
			default:
			    $value = false;
		    }
		} else {
		    $value = false;
		}
		break;
	    case 'rpi':
		switch($sensor['subtype']) {
		    case 'thermo':
			$file = '/sys/class/thermal/thermal_zone0/temp';
			if(file_exists($file)) {
			    $value = trim(file_get_contents($file)) / 1000;
			} else {
			    $value = false;
			}
			break;
		    default:
			$value = false;
		}
		break;
	    case 'remote':
		switch($sensor['subtype']) {
		    case 'json':
			error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
			$req = new HTTP_Request($sensor['url']);
			$req->sendRequest();
			if (!PEAR::isError($req)) {
			    $json  = $req->getResponseBody();
			    $ret   = json_decode($json, true);
			    $value = $ret['value'];
			} else {
			    $value = false;
			}
			break;
		    case 'owhttpd':
		    case 'owfs':
			$opts = array(
			    'http'=>array(
				'method'=>"GET",
				'header'=>"Accept-language: en\r\n" .
				    "Cookie: foo=bar\r\n"
			    )
			);
			$res = file_get_contents($sensor['uri'], false, stream_context_create($opts));
			if ($res === false) {
			    $value = false;
			} else {
			    $matches = array();
			    preg_match('/<\/TD><TD>\s+([0-9\.]+)<\/TD><\/TR>/', $res, $matches);
			    $value = $matches[1];
			}
			break;
		    default:
			$value = false;
		}
		break;
	    default:
		$value = false;
	}
	return $value;
    }

    function homeSensorGetValue($key) {
	global $conf, $cache;
	$value = homeCacheGet('sensor', 'value', $key);
	if(homeCacheFound()) {
	    return $value;
	} else {
	    return $conf['sensor'][$key]['default'];
	}
    }

    function homeSensorGetUpdateUser($key) {
	global $conf, $cache;
	$value = homeCacheGet('sensor', 'update-user', $key);
	if(homeCacheFound()) {
	    return $value;
	} else {
	    return false;
	}
    }

    function homeSensorGetUpdateTS($key) {
	global $conf, $cache;
	$value = homeCacheGet('sensor', 'update-ts', $key);
	if(homeCacheFound()) {
	    return $value;
	} else {
	    return false;
	}
    }

    function homeSensorGetArch($key) {
	global $conf, $cache;
	$json = homeCacheGet('sensor', 'arch', $key);
	if(homeCacheFound()) {
	    return json_decode($json, true);
	} else {
	    return array();
	}
    }

    function homeSensorSetValue($key, $value) {
	global $conf;
		homeCacheArch('sensor',                $key, $value   );
		homeCacheSet ('sensor', 'update-user', $key, 'system' );
		homeCacheSet ('sensor', 'update-ts',   $key, date('U'));
	return	homeCacheSet ('sensor', 'value',       $key, $value   );
    }

    function homeSensorDeleteValue($key) {
	global $conf, $cache;
		homeCacheDelete('sensor', 'update-user', $key);
		homeCacheDelete('sensor', 'update-ts',   $key);
	return	homeCacheDelete('sensor', 'value',       $key);
    }
?>